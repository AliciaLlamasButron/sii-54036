#pragma once

#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Esfera.h"
#include "DatosMemCompartida.h"
#include "MundoCliente.h"

int main(int argc, char const **argv)
{
	char *orgbot;
	DatosMemCompartida *datosmpunt;

	int fdatos=open("/tmp/datos.txt",O_RDWR,0777);
	if(fdatos<0) {
		perror("Open datos");
	}
	orgbot=(char*)mmap(NULL,sizeof(*(datosmpunt)),PROT_WRITE | PROT_READ,MAP_SHARED,fdatos,0);
	close(fdatos);
	datosmpunt=(DatosMemCompartida*)orgbot;

	while(1)
	{

		float posiciony = (datosmpunt->raqueta1.y2+datosmpunt->raqueta1.y1)/2;

		if(datosmpunt->bot)
			exit(0);
		else if (posiciony<datosmpunt->esfera.centro.y)
			datosmpunt->accion=1;
		else if (posiciony>datosmpunt->esfera.centro.y)
			datosmpunt->accion=-1;
		else
			datosmpunt->accion=0;

		usleep(25000);
	}

	munmap(orgbot,sizeof(*(datosmpunt)));
	return 0;
}

