// Autor:Alicia Llamas Butron

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


int main(int argc, char **argv)
{
	int n_read;
	char buffer[40];
	if(mkfifo("/tmp/Tuberia", 0777)!=0)
		printf ("Hubo un problema al crear la tuberia pipe\n");

	int mipipe=open("/tmp/Tuberia", O_RDONLY);
        if(mipipe<0)
        {
                perror("open");
                return 1;
        }

	while((n_read=read(mipipe, buffer, 40))>0) //Mientras haya qué leer
	{
		printf("%s", buffer);
	}
	if(n_read<0)
	{
                perror("read");
                return 1;
        }

	close(mipipe);

	int ret = unlink("/tmp/Tuberia");
	if(ret<0)
	{
                perror("unlink");
                return 1;
        }

	return 0;
}

