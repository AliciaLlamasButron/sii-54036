//Autor: Alicia Llamas Butron
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <iostream>
#include <signal.h>


//Añadimos la función Hilo_Comandos.

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void Recepcion (int n){  //Recepcion de las señales
	if (n==SIGINT || n==SIGPIPE || n==SIGTERM){  //El proceso no ha ido bien
		printf ("El proceso ha fallado por la señal %d \n", n);
		exit (n);
	}
	if (n==SIGUSR2){  //El proceso ha ido bien
		printf ("El proceso ha terminado correctamente \n");
                exit (0);
	}
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cadena[100];
            //read(tubtecla, cadena, sizeof(cadena));
	    s_comunicacion.Receive(cadena,sizeof(cadena));
            unsigned char key;
            sscanf(cadena,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(tuberia);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

        //Se dibujan las esferas.
	for (i=0; i<esfera.size(); i++)
		esfera[i].Dibuja();

        //Se crea un vector disparo de cada jugador
	for (i=0; i<disparos1.size(); i++)//Vector de disparo para el primer jugador
		disparos1[i].Dibuja();
	for (i=0;i<disparos2.size();i++)//Vector de disparo para el segundo jugador
		disparos2[i].Dibuja();

	////////////////////////////////
	//	AQUI TERMINA MI DIBUJO
	///////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	int i,j;

	for (i=0;i<esfera.size();i++)//Bucle para mover las esferas
		esfera[i].Mueve(0.025f);

	for(i=0;i<paredes.size();i++)
	{
		for (j=0;j<esfera.size();j++)//Rebote de las esferas con la pared
			paredes[i].Rebota(esfera[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for (i=0;i<disparos1.size();i++)//Movimiento disparos primer jugador
		disparos1[i].Mueve(0.025f);
	for (i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);//Movimiento disparos segundo jugador
	
	for (i=0;i<esfera.size();i++){
		jugador1.Rebota(esfera[i]);//Rebote de esfera con jugador1
		jugador2.Rebota(esfera[i]);//Rebote de esfera con jugador2
	}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fondo_izq
		if(fondo_izq.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;

			sprintf(msj,"Jugador 1 lleva de %d puntos\n", puntos1);
			int write1 = write(tuberia,msj,100);
			if(write1==-1)
				perror("Error al escribir en Tuberia");
        		sprintf(msj,"Jugador 2 lleva %d puntos\n", puntos2);
			int write2 = write(tuberia,msj,100);
			if(write2==-1)
				perror("Error al escribir en Tuberia");
			}
		}

	for(i=0;i<esfera.size();i++){//Rebote de esfera con fonfo_dcho
		if(fondo_dcho.Rebota(esfera[i]))
		{
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;

			sprintf(msj,"Jugador 1 lleva %d puntos\n", puntos1);
			int write1 = write(tuberia,msj,100);
			if(write1==-1)
				perror("Error al escribir en Tuberia");
        		sprintf(msj,"Jugador 2 lleva %d puntos\n", puntos2);
			int write2 = write(tuberia,msj,100);
			if(write2==-1)
				perror("Error al escribir en Tuberia");
		}
	}

	//Para adaptarnos a la práctica4 no se van a generar más esferas

	//Impacto de los disparos del jugador1 contra la raqueta del jugador 2
	for(int i=0;i<disparos1.size();i++)
	{
		if(jugador2.Rebota(disparos1[i]))
		{
			if((jugador2.y2-jugador2.y1)>1.5)
			{
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		}
	}
	//Impacto de los disparos del jugador2 contra la raqueta del jugador 1
	for(int i=0;i<disparos2.size();i++)
	{
		if(jugador1.Rebota(disparos2[i]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		}
	}

	//El juego se termina cuando uno de los jugadores llega a 3 puntos
	if ((puntos1 == 3) || (puntos2==3))
		kill(getpid(),SIGUSR2); //Igual que exit(0)

	//Comunicación a través de sockets
	sprintf(mensaje,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera[0].centro.x,esfera[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1, puntos2);
	s_comunicacion.Send(mensaje,sizeof(mensaje));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'c': //Se realiza el disparo del jugador1
		if(disparos1.size()==0){
			Esfera e;
			e.radio=0.2;
			e.centro.x=jugador1.x1+0.6;
			e.centro.y=jugador1.y1+(jugador1.y2-jugador1.y1)/2;
			e.velocidad.y=0;
			e.velocidad.x=3;
			e.rojo=255;
			e.verde=0;
			e.azul=0;
			disparos1.push_back(e);
		}
		break;
	case 'm'://Se realiza el disparo del jugador2
                if(disparos2.size()==0){
                        Esfera e;
                        e.radio=0.2;
                        e.centro.x=jugador2.x1-0.6;
                        e.centro.y=jugador2.y1+(jugador2.y2-jugador2.y1)/2;
                        e.velocidad.y=0;
                        e.velocidad.x=-3;
                        e.rojo=0;
                        e.verde=255;
                        e.azul=0;
                        disparos2.push_back(e);
		}
	}*/
}

void CMundo::Init()
{
        //Esfera
        Esfera e;
        e.radio=0.5;
        e.centro.x=0;
        e.centro.y=rand()/(float)RAND_MAX;
        e.velocidad.x=2+2*rand()/(float)RAND_MAX;
        e.velocidad.y=2+2*rand()/(float)RAND_MAX;
        esfera.push_back(e);

	Plano p;

	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Tubería logger.
	tuberia = open("/tmp/Tuberia",O_WRONLY,0777);
	if (tuberia<0)
		perror("Open Tuberia Servidor");

	//Conexión del Socket
	char ip[]="127.0.0.1";
	if(s_conexion.InitServer(ip,8000)==-1)
		printf("error abriendo el servidor\n");
	s_comunicacion=s_conexion.Accept();

	//Se recibe el nombre del cliente
	char nombre[50];
	s_comunicacion.Receive(nombre,sizeof(nombre));
	printf("%s se ha conectado a la partida.\n",nombre);

	int proceso = pthread_create(&thid1, NULL, hilo_comandos, this);
	if(proceso!=0)
		perror("Error en pthread");

	//Tratamiento de señales
	struct sigaction act;  //Estructura para las señales
	act.sa_flags=SA_RESTART;  
	act.sa_handler=&Recepcion;  //Apunta a la función
	sigaction(SIGINT, &act, NULL);  //Declaración de señales
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGPIPE, &act, NULL);
	sigaction(SIGUSR2, &act, NULL);
}
